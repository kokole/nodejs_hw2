const controller = require('../controllers/user.controller');
const verifyToken = require('../middlewares/authJwt');

module.exports = function(app) {
	app.get(
		'/api/users/me',
		verifyToken,
		controller.getProfile
	);

  app.patch(
    '/api/users/me',
    verifyToken,
    controller.changePassword
  );

  app.delete(
    '/api/users/me',
    verifyToken,
    controller.deleteProfile
  );
};