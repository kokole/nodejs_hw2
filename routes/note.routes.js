const verifyToken = require('../middlewares/authJwt');
const controller = require('../controllers/note.controller');

module.exports = function(app) {
	app.post(
		'/api/notes',
		verifyToken,
		controller.createNote
	);

	app.delete(
		'/api/notes/:id',
		verifyToken,
		controller.deleteNote
	);

	app.get(
		'/api/notes',
		verifyToken,
		controller.viewNotes
	);

	app.get(
		`/api/notes/:id`,
		verifyToken,
		controller.getNoteById
	);

	app.patch(
		'/api/notes/:id',
		verifyToken,
		controller.toggleNoteCompletion
	);

  app.put(
    '/api/notes/:id',
    verifyToken,
    controller.updateNote
  );
};