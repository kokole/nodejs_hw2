const Credentials = require('../models/User.js');
let bcrypt = require('bcryptjs');

exports.getProfile = (req,res) => {
	Credentials.findById(req.userId).exec((err, user) => {
		if (err) {
			res.status(500).send({message: 'Error message'});
			return;
		}

		if (!user) {
			res.status(400).send({message: 'Error message'});
			return;
		}

		res.status(200).send({
			user: {
				_id: user._id,
				username: user.username,
				createdDate: user.createdDate,
			}
		})
	})
}

exports.changePassword = (req, res) => {
  Credentials.findById(req.userId).exec((err, user) => {
    let passwordIsValid = bcrypt.compareSync(
      req.body.oldPassword,
      user.password
    );
    if (!passwordIsValid) {
      return res.status(400).send({
        message: 'Invalid Old Password!'
      });
    };
    user.password = bcrypt.hashSync(req.body.newPassword);
    user.save((err, user) => {
      if (err) {
        res.status(500).send({message: 'Error message'})
        return;
      }
      return res.status(200).json({message: 'Success'})
    });
  })
}

exports.deleteProfile = (req, res) => {
  Credentials.deleteOne({userId: req.userId}).exec((err, user) => {
    if (err) {
      res.status(500).send({message: 'Error message'});
      return;
    }

    if (!user) {
      res.status(400).send({message: 'User not found'});
      return;
    }

    res.status(200).send({
      message: 'Success'
    });
  })
}