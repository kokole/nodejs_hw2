const secret = process.env.SECRET;
const Credentials = require('../models/User.js');

let jwt = require('jsonwebtoken');
let bcrypt = require('bcryptjs');

exports.register =  (req, res) => {
	const newUser = new Credentials({
		username: req.body.username,
		password: bcrypt.hashSync(req.body.password),
	});

	newUser.save((err, user) => {
		if (err) {
			res.status(500).send({message: 'Error message', err})
			return;
		}

		return res.status(200).json({message: 'Success'})
	});
}

exports.login = (req, res) => {
	Credentials.findOne({
		username: req.body.username
	})
		.exec((err, user) => {
			if (err) {
				res.status(500).send({message: 'Error message'})
				return
			}

			if (!user) {
				return res.status(400).send({message: 'User Not found'});
			}

			let passwordIsValid = bcrypt.compareSync(
				req.body.password,
				user.password
			);

			if (!passwordIsValid) {
				return res.status(400).send({
					message: 'Invalid Password!'
				});
			}

			let token = jwt.sign({id: user.id}, secret, {
				expiresIn: 86400 //24 hours
			});

			res.status(200).send({
				message: 'Success',
				jwt_token: `${token}`
			})
		})
}