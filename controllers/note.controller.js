const Notes = require('../models/Notes.js');

exports.createNote = (req, res) => {
	const newNote = new Notes({
		userId: req.userId,
		completed: false,
		text: req.body.text, 
	})

	newNote.save((err, note) => {
		if (err) {
			res.status(500).send({message: 'Error with creating note'})
			return;
		}

		if (!note) {
			res.status(400).send({message: 'Error with creating note'})
			return;
		}

		return res.status(200).json({message: 'Success'})
	})
}

exports.deleteNote = (req, res) => {
	Notes.deleteOne(
		{
			userId: req.userId,
			_id: req.params.id,
		}
	)
	.exec((err, doc) => {
		if (err) {
			res.status(500).send({message: 'Error message: ', err})
			return;
		}
		if (!doc) {
			res.status(400).send({message: 'Document not found'})
			return;
		}
		res.status(200).send({message: 'Success'})
	})
}

exports.viewNotes = (req,res) => {
	Notes.find(
		{ userId: req.userId },
		null,
		{ 
			skip: parseInt(req.query.offset, 10) || 0,
			limit: parseInt(req.query.limit, 10) || 0 
		}
	) 
	.exec((err, docs) => {
		if (err) {
			res.status(500).send({message: "Error message"})
			return;
		}

		if (!docs) {
			res.status(400).send({message: 'Documents not found'})
			return;
		}

		res.status(200).send({
			offset: req.query.offset,
			limit: req.query.limit,
			count: docs.length,
			notes: docs,
		});
	})
}

exports.getNoteById = (req, res) => {
	Notes.findOne(
		{
			userId: req.userId,
			_id: req.params.id,
		}
	)
	.exec((err, doc) => {
		if (err) {
			res.status(500).send({message: 'Error message: ', err})
			return;
		}

		if (!doc) {
			res.status(400).send({message: 'Document not found'})
			return;
		}
    console.log(doc);
		res.status(200).send(
      { 
        note: {
          _id: doc._id,
          userId: req.userId,
          completed: doc.completed,
          text: doc.text,
          createdDate: doc.createdDate 
        }
      }
    );
	})
}

exports.toggleNoteCompletion = (req, res) => {
	Notes.findOneAndUpdate(
		{
			userId: req.userId,
			_id: req.params.id,
		}, 
		[{
			$set: { completed: {$eq: [false, "$completed"]}} 
		}],
		{
			new: true,
		},
		(err, doc) => {
			if (err) {
				res.status(500).send({message: 'Error message: ', err})
				return;
			}

			if (!doc) {
				res.status(400).send({message: 'Document cant be updated'})
				return;
			}

			res.status(200).send({message: 'Success'});
		}
	)
}

exports.updateNote = (req, res) => {
  Notes.findOneAndUpdate(
    {
      userId: req.userId,
      _id: req.params.id,
    }, 
    req.body,
    {
      new: true,
    },
    (err, doc) => {
      if (err) {
        res.status(500).send({message: 'Error message: ', err})
        return;
      }

      if (!doc) {
        res.status(400).send({message: 'Document cant be updated'})
        return;
      }

      res.status(200).send({message: 'Success'});
    }
  )
}