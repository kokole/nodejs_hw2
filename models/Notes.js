const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NoteSchema = new Schema({
	userId: String,
	completed: Boolean,
	text: String,
	createdDate: {
		type: String,
		default: (new Date(Date.now())).toISOString(),
	}
})

module.exports = Notes = mongoose.model('Notes', NoteSchema);