const jwt = require("jsonwebtoken");
const secret = process.env.SECRET;
const Credentials = require('../models/User');

verifyToken = (req, res, next) => {
  let raw_token = req.headers.authorization;
  let token = raw_token.split(' ')[1];
  if (!token) {
    return res.status(400).send({ message: "No token provided!" });
  }

  jwt.verify(token, secret, (err, decoded) => {
    if (err) {
      return res.status(400).send({ message: "Unauthorized!" });
    }
    req.userId = decoded.id;
    next();
  });
};

module.exports = verifyToken; //authJwt